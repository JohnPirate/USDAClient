# USDAClient

## Description

This is a client for the USDA food api.

## Usage

### Basic client ussage
```php
// You need a registrated api key
$usda_api_key = 'YOUR_API_KEY';

// New client instance
$usda_client = new \USDAClient\Client($usda_api_key);
```

### Basic food search
```php
$search_request = new \USDAClient\Entities\SearchRequestEntity();
$search_request->setQuery('strawberries');
$search_request->setMax(20);

// Perform a search call
$response = $usda_client->call($search_request);
```

### Basic food report
```php
$report_request = new \USDAClient\Entities\ReportRequestEntity();
$report_request->setNdbno("45130960");
// Request a full report
// $report_request->fullReport();

// Perform a report call
$response = $usda_client->call($report_request);
```