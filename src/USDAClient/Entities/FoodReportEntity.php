<?php

namespace USDAClient\Entities;

use USDAClient\Exceptions\EntityException;

/**
 * Class FoodReportEntity
 * @package USDAClient\Entities
 *
 * @link https://ndb.nal.usda.gov/ndb/doc/apilist/API-FOOD-REPORT.md
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class FoodReportEntity implements \USDAClient\Entities\ResponseInterface
{
    const DS_BRANDED_FOOD_PRODUCTS = 'Branded Food Products';
    const DS_STANDARD_REFERENCE = 'Standard Reference';

    /**
     * Release version of the data being reported
     * ('sr' from response)
     *
     * @var string
     */
    public $standard_release;

    /**
     * Report type
     * ('type' from response)
     *
     * @var string
     */
    public $type;

    /**
     * NDB data number
     * ('ndbno' from response)
     *
     * @var string
     */
    public $ndbno;

    /**
     * Food name
     * ('name' from response)
     *
     * @var string
     */
    public $name;

    /**
     * Short description
     * ('sd' from response)
     *
     * @var string
     */
    public $short_description;

    /**
     * Food group
     * ('fg' from response)
     *
     * @var string
     */
    public $group;

    /**
     * Scientific name
     * ('sn' from response)
     *
     * @var string
     */
    public $scientific_name;

    /**
     * Commercial name
     * ('cn' from response)
     *
     * @var string
     */
    public $commercial_name;

    /**
     * Manufacturer
     * ('manu' from response)
     *
     * @var string
     */
    public $manufacturer;

    /**
     * Nitrogen to protein conversion factor
     * ('nf' from response)
     *
     * @var float
     */
    public $nitrogen_factor;

    /**
     * Carbohydrate factor
     * ('cf' from response)
     *
     * @var float
     */
    public $carbohydrate_factor;

    /**
     * Fat factor
     * ('ff' from response)
     *
     * @var float
     */
    public $fat_factor;

    /**
     * Protein factor
     * ('pf' from response)
     *
     * @var float
     */
    public $protein_factor;

    /**
     * Refuse %
     * ('r' from response)
     *
     * @var float
     */
    public $refuse;

    /**
     * Refuse description
     * ('rd' from response)
     *
     * @var string
     */
    public $refuse_description;

    /**
     * Database source: 'Branded Food Products' or 'Standard Reference'
     * ('ds' from response)
     *
     * @var string
     */
    public $database_source;

    /**
     * List of ingredients
     * ('ing.desc' from response)
     *
     * @var array
     */
    public $ingredients = [];

    /**
     * Date ingredients were last updated by company
     * ('ing.upd' from request)
     *
     * @var string
     */
    public $ingredients_update;

    /**
     * @var \USDAClient\Entities\FoodNutrientEntity[]
     */
    public $nutrients = [];

    /**
     * FoodReportEntity constructor.
     *
     * @param null|object $data
     */
    public function __construct($data = null)
    {
        if (isset($data)) {
            $this->standard_release = $data['sr'];
            $this->type = $data['type'];
            $this->init($data['food']);
            foreach ($data['food']['nutrients'] as $value) {
                $nutrient = new FoodNutrientEntity($value);
                $this->addNutrient($nutrient);
            }
        }
    }

    /**
     * @param array $data
     *
     * @throws \USDAClient\Exceptions\EntityException
     */
    protected function init($data)
    {
        if (!isset($data['ndbno'])) {
            throw new EntityException('Food has no NDB number!');
        }
        $this->ndbno = $data['ndbno'];
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->short_description = isset($data['sd']) ? $data['sd'] : null;
        $this->group = isset($data['fg']) ? $data['fg'] : null;
        $this->scientific_name = isset($data['sn']) ? $data['sn'] : null;
        $this->commercial_name = isset($data['cn']) ? $data['cn'] : null;
        $this->manufacturer = isset($data['manu']) ? $data['manu'] : null;
        $this->nitrogen_factor = isset($data['nf']) ? $data['nf'] : null;
        $this->carbohydrate_factor = isset($data['cf']) ? $data['cf'] : null;
        $this->fat_factor = isset($data['ff']) ? $data['ff'] : null;
        $this->protein_factor = isset($data['pf']) ? $data['pf'] : null;
        $this->refuse = isset($data['r']) ? $data['r'] : null;
        $this->refuse_description = isset($data['rd']) ? $data['rd'] : null;
        $this->database_source = isset($data['ds']) ? $data['ds'] : null;

        if ($this->isBrandedFood()) {
            $this->ingredients_update = isset($data['ing']['upd']) ? $data['ing']['upd'] : null;
            $this->ingredients = isset($data['ing']['desc']) ? explode(',', $data['ing']['desc']) : [];
            $this->ingredients = array_map(function ($ingredient) {
                return strtolower(trim($ingredient, " ."));
            }, $this->ingredients);
        }
    }

    /**
     * @param FoodNutrientEntity|null $nutrient
     */
    public function addNutrient(FoodNutrientEntity $nutrient = null)
    {
        if (isset($nutrient)) {
            array_push($this->nutrients, $nutrient);
        }
    }

    /**
     * Returns the object data as an array
     *
     * @return array
     */
    public function toArray()
    {
        $food = [
            'standard_release' => $this->standard_release,
            'type' => $this->type,
            'ndbno' => $this->ndbno,
            'name' => $this->name,
            'short_description' => $this->short_description,
            'group' => $this->group,
            'scientific_name' => $this->scientific_name,
            'commercial_name' => $this->commercial_name,
            'manufacturer' => $this->manufacturer,
            'nitrogen_factor' => $this->nitrogen_factor,
            'carbohydrate_factor' => $this->carbohydrate_factor,
            'fat_factor' => $this->fat_factor,
            'protein_factor' => $this->protein_factor,
            'refuse' => $this->refuse,
            'refuse_description' => $this->refuse_description,
            'database_source' => $this->database_source,
            'nutrients' => [],
            'ingredients' => $this->ingredients,
            'ingredients_update' => $this->ingredients_update,
        ];

        foreach ($this->nutrients as $nutrient) {
            $food['nutrients'][] = $nutrient->toArray();
        }

        return $food;
    }

    /**
     * Checks wheter food is branded or not
     *
     * @return bool
     */
    public function isBrandedFood()
    {
        return $this->database_source === self::DS_BRANDED_FOOD_PRODUCTS;
    }

    /**
     * Checks wheter food is a standard food or not
     *
     * @return bool
     */
    public function isStandardFood()
    {
        return $this->database_source === self::DS_STANDARD_REFERENCE;
    }
}
