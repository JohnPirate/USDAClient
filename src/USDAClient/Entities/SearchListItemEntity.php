<?php

namespace USDAClient\Entities;

/**
 * Class SearchListItemEntity
 * @package USDAClient\Entities
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class SearchListItemEntity implements \USDAClient\Entities\ResponseInterface
{
    /**
     * Beginning offset into the results list for the items in the list requested
     * ('offset' from request)
     *
     * @var int
     */
    protected $offset;

    /**
     * Food group to which the food belongs
     * ('group' from request)
     *
     * @var string
     */
    protected $group;

    /**
     * The food’s name
     * ('name' from request)
     *
     * @var string
     */
    protected $name;

    /**
     * The food’s NDB Number
     * ('ndbno' from request)
     * @var string
     */
    protected $ndbno;

    /**
     * Data source: BL := Branded Food Products or SR := Standard Release
     * ('ds' from response)
     *
     * @var string
     */
    protected $data_source;

    /**
     * SearchListItemEntity constructor.
     *
     * @param $offset
     * @param $group
     * @param $name
     * @param $ndbno
     * @param $data_source
     */
    public function __construct($offset, $group, $name, $ndbno, $data_source)
    {
        $this->offset = $offset;
        $this->group = $group;
        $this->name = $name;
        $this->ndbno = $ndbno;
        $this->data_source = $data_source;
    }

    /**
     * Returns the object data as an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'offset' => $this->offset,
            'group' => $this->group,
            'name' => $this->name,
            'ndbno' => $this->ndbno,
            'data_source' => $this->data_source
        ];
    }

    /**
     * @return int
     */
    public function getOffset ()
    {
        return $this->offset;
    }

    /**
     * @return string
     */
    public function getGroup ()
    {
        return $this->group;
    }

    /**
     * @return string
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNdbno ()
    {
        return $this->ndbno;
    }

    /**
     * @return string
     */
    public function getDataSource ()
    {
        return $this->data_source;
    }
}