<?php

namespace USDAClient\Entities;

/**
 * Class FoodNutrientEntity
 * @package USDAClient\Entities
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class FoodNutrientEntity
{
    /**
     * Nutrient number (nutrient_no) for the nutrient
     * ('nutrient_id' from response)
     *
     * @var integer
     */
    public $nutrient_id;

    /**
     * Nutrient name
     * ('name' from response)
     *
     * @var string
     */
    public $name;

    /**
     * Nutrient group
     * ('group' from response)
     *
     * @var string
     */
    public $group;

    /**
     * Unit of measure for this nutrient
     * ('unit' from response)
     *
     * @var string
     */
    public $unit;

    /**
     * 100 g equivalent value of the nutrient
     * ('value' from response)
     *
     * @var float
     */
    public $value;

    /**
     * List of source id's in the sources list referenced for this nutrient.
     * ('sourcecode' from response)
     * @see \USDAClient\Entities\FoodReportEntity -> $sources
     *
     * @var array
     */
    public $sourcecodes = [];

    /**
     * Number of data points
     * ('dp' from response)
     *
     * @var int
     */
    public $data_points;

    /**
     * Standard error
     * ('se' from response)
     *
     * @var float
     */
    public $standard_error;

    /**
     * FoodNutrientEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->init($data);
    }

    /**
     * @param array $data
     */
    protected function init(array $data)
    {
        if (!isset($data['nutrient_id'])) {
            throw new EntityException('Nutrient has no ID!');
        }
        $this->nutrient_id = $data['nutrient_id'];
        $this->name =  isset($data['name']) ? $data['name'] : null;
        $this->group = isset($data['group']) ? $data['group'] : null;
        $this->unit = isset($data['unit']) ? $data['unit'] : null;
        $this->value = isset($data['value']) ? $data['value'] : null;
        $this->sourcecodes = isset($data['sourcecode']) ? $data['sourcecode'] : null;
        $this->data_points = isset($data['dp']) ? $data['dp'] : null;
        $this->standard_error = isset($data['se']) ? $data['se'] : null;
    }

    /**
     * Returns the object data as an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'nutrient_id' => $this->nutrient_id,
            'name' => $this->name,
            'group' => $this->group,
            'unit' => $this->unit,
            'value' => $this->value,
            'sourcecodes' => $this->sourcecodes,
            'data_points' => $this->data_points,
            'standard_error' => $this->standard_error
        ];
    }
}
