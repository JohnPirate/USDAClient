<?php

namespace USDAClient\Entities;


/**
 * Interface ResponseInterface
 * @package USDAClient\Entities
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
interface ResponseInterface
{
    /**
     * Returns the object data as an array
     *
     * @return array
     */
    public function toArray();
}