<?php

namespace USDAClient\Entities;

/**
 * Interface RequestInterface
 * @package USDAClient\Entities
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
interface RequestInterface
{
    /**
     * Creates the query string for the request url
     *
     * @return string
     */
    public function getRequestParams();
}