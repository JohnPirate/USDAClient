<?php

namespace USDAClient\Entities;

/**
 * Class SearchRequestEntity
 * @package USDAClient\Entities
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class SearchRequestEntity implements \USDAClient\Entities\RequestInterface
{
    const SORT_BY_NAME = 'n';
    const SORT_BY_RELEVANCE = 'r';
    const FORMAT_JSON = 'json';
    const FORMAT_XML = 'xml';

    /**
     * Search terms
     * ('q' at request param)
     *
     * @var string
     */
    protected $query;

    /**
     * Data source. Must be either 'Branded Food Products' or 'Standard Reference'
     * ('ds' at request param)
     *
     * @var string
     */
    protected $data_source;

    /**
     * Food group
     * ('fg' at request param)
     *
     * @var string
     */
    protected $food_group;

    /**
     * Sort the results by food name (n) or by search relevance (r)
     * ('sort' at request param)
     *
     * @var string
     */
    protected $sort;

    /**
     * Maximum rows to return
     * ('max' at request param)
     *
     * @var int
     */
    protected $max;

    /**
     * Beginning row in the result set to begin
     * ('offset' at request param)
     *
     * @var int
     */
    protected $offset;

    /**
     * results format: json or xml
     * ('format' at request param)
     *
     * @var string
     */
    protected $format;

    /**
     * Default request params
     *
     * @var array
     */
    protected $default_request_params = [
        'query' => '',                      // q
        'max' => 50,                        // max
        'offset' => 0,                      // offset
        'format' => self::FORMAT_JSON,      // format
        'sort' => self::SORT_BY_RELEVANCE,  // sort
        'data_source' => '',                // ds
        'food_group' => ''                  // fg
    ];

    /**
     * SearchRequestEntity constructor.
     */
    public function __construct()
    {
    }

    /**
     * Creates the query string for the request url
     *
     * @return string
     */
    public function getRequestParams()
    {
        extract($this->toArrayDefaults());

        return sprintf('q=%s&max=%s&offset=%s&format=%s&sort=%s&ds=%s&fg=%s',
            $query,
            $max,
            $offset,
            $format,
            $sort,
            $data_source,
            $food_group
        );
    }

    /**
     * Returns all attributes as an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'query' => $this->query,
            'max' => $this->max,
            'offset' => $this->offset,
            'format' => $this->format,
            'sort' => $this->sort,
            'data_source' => $this->data_source,
            'food_group' => $this->food_group
        ];
    }

    /**
     * Same as toArray() but with default values
     *
     * @return array
     */
    public function toArrayDefaults()
    {
        $params = array_filter($this->toArray(), function ($param) {
            if ($param) {
                return $param;
            }
        });
        return array_merge($this->default_request_params, $params);
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getDataSource()
    {
        return $this->data_source;
    }

    /**
     * @param string $data_source
     */
    public function setDataSource($data_source)
    {
        $this->data_source = $data_source;
    }

    /**
     * @return string
     */
    public function getFoodGroup()
    {
        return $this->food_group;
    }

    /**
     * @param string $food_group
     */
    public function setFoodGroup($food_group)
    {
        $this->food_group = $food_group;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }
}