<?php

namespace USDAClient\Entities;

/**
 * Class ReportRequestEntity
 * @package USDAClient\Entities
 *
 * @link https://ndb.nal.usda.gov/ndb/doc/apilist/API-FOOD-REPORT.md
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class ReportRequestEntity implements \USDAClient\Entities\RequestInterface
{
    const REPORT_TYPE_BASIC = 'b';
    const REPORT_TYPE_FULL = 'f';
    const REPORT_TYPE_STATS = 's';
    const FORMAT_JSON = 'json';
    const FORMAT_XML = 'xml';

    /**
     * NDB no
     * ('ndbno' at request param)
     *
     * @var string
     */
    protected $ndbno;

    /**
     * Report type: [b]asic or [f]ull or [s]tats
     * ('type' at request param)
     *
     * @var string
     */
    protected $type;

    /**
     * Report format: xml or json
     * ('format' at request param)
     *
     * @var string
     */
    protected $format;

    /**
     * Default request params
     *
     * @var array
     */
    protected $default_request_params = [
        'ndbno' => '',
        'type' => self::REPORT_TYPE_BASIC,
        'format' => self::FORMAT_JSON
    ];

    /**
     * ReportRequestEntity constructor.
     */
    public function __construct()
    {
    }

    /**
     * Creates the query string for the request url
     *
     * @return string
     */
    public function getRequestParams()
    {
        extract($this->toArrayDefaults());

        return sprintf('ndbno=%s&type=%s&format=%s',
            $ndbno,
            $type,
            $format
        );
    }

    /**
     * Returns all attributes as an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'ndbno' => $this->ndbno,
            'type' => $this->type,
            'format' => $this->format
        ];
    }

    /**
     * Same as toArray() but with default values
     *
     * @return array
     */
    public function toArrayDefaults()
    {
        $params = array_filter($this->toArray(), function ($param) {
            if ($param) {
                return $param;
            }
        });
        return array_merge($this->default_request_params, $params);
    }

    /**
     * Set report type to basic
     */
    public function basicReport()
    {
        $this->type = self::REPORT_TYPE_BASIC;
    }

    /**
     * Set report type to full
     */
    public function fullReport()
    {
        $this->type = self::REPORT_TYPE_FULL;
    }

    /**
     * Set report type to stats
     */
    public function statsReport()
    {
        $this->type = self::REPORT_TYPE_STATS;
    }

    /**
     * @return string
     */
    public function getNdbno()
    {
        return $this->ndbno;
    }

    /**
     * @param string $ndbno
     */
    public function setNdbno($ndbno)
    {
        $this->ndbno = $ndbno;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }
}