<?php

namespace USDAClient\Entities;

use \USDAClient\Entities\SearchListItemEntity;


/**
 * Class SearchResponseEntity
 * @package USDAClient\Entities
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class SearchResponseEntity implements \USDAClient\Entities\ResponseInterface
{
    /**
     * Terms requested and used in the search
     * ('q' from response)
     *
     * @var string
     */
    protected $query;

    /**
     * Standard Release version of the data being reported
     * ('sr' from response)
     *
     * @var string
     */
    protected $standard_release;

    /**
     * Data source: BL := Branded Food Products or SR := Standard Release
     * ('ds' from response)
     *
     * @var string
     */
    protected $data_source;

    /**
     * Beginning item in the list
     * ('start' from response)
     *
     * @var int
     */
    protected $start;

    /**
     * Last item in the list
     * ('end' from response)
     *
     * @var int
     */
    protected $end;

    /**
     * Total number of items returned by the search
     * ('total' from response)
     *
     * @var int
     */
    protected $total;

    /**
     * Food group to which the food belongs
     * ('group' from request)
     *
     * @var string
     */
    protected $group;

    /**
     * Requested sort order (r := relevance or n := name)
     * ('sort' from request)
     * @var string
     */
    protected $sort;

    /**
     * List of requested food items
     * ('list' from request)
     *
     * @var \USDAClient\Entities\SearchListItemEntity[]
     */
    protected $list_items = array();

    /**
     * SearchResponseEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->standard_release = $data['sr'];
        $this->data_source = $data['ds'];
        $this->start = $data['start'];
        $this->end = $data['end'];
        $this->total = $data['total'];
        $this->group = $data['group'];
        $this->sort = $data['sort'];

        if (isset($data['item']) && count($data['item'])) {
            $this->createItems($data['item']);
        }
    }

    /**
     * Creates the items from the request data
     *
     * @param array $items
     */
    protected function createItems(array $items)
    {
        foreach ($items as $item) {
            $this->list_items[] = new SearchListItemEntity(
                $item['offset'],
                $item['group'],
                $item['name'],
                $item['ndbno'],
                $item['ds']
            );
        }
    }

    /**
     * Returns the object data as an array
     *
     * @return array
     */
    public function toArray()
    {
        $search = [
            'query' => $this->query,
            'standard_release' => $this->standard_release,
            'data_source' => $this->data_source,
            'start' => $this->start,
            'end' => $this->end,
            'total' => $this->total,
            'group' => $this->group,
            'sort' => $this->sort,
            'list_items' => []
        ];

        foreach ($this->list_items as $item)
        {
            $search['list_items'][] = $item->toArray();
        }

        return $search;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return string
     */
    public function getStandardRelease()
    {
        return $this->standard_release;
    }

    /**
     * @return string
     */
    public function getDataSource()
    {
        return $this->data_source;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @return int
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return \USDAClient\Entities\SearchListItemEntity[]
     */
    public function getListItems()
    {
        return $this->list_items;
    }
}