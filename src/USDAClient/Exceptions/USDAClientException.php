<?php

namespace USDAClient\Exceptions;


/**
 * Class USDAClientException
 * @package USDAClient\Exceptions
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class USDAClientException extends \Exception
{
}