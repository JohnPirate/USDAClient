<?php

namespace USDAClient\Exceptions;


/**
 * Class EntityException
 * @package USDAClient\Exceptions
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class EntityException extends \Exception
{
}