<?php

namespace USDAClient;

use \GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use USDAClient\Entities\FoodReportEntity;
use USDAClient\Entities\ReportRequestEntity;
use USDAClient\Entities\SearchResponseEntity;
use USDAClient\Entities\SearchRequestEntity;
use USDAClient\Exceptions\USDAClientException;


/**
 * Class Client
 * @package USDAClient
 *
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 *
 * @version 0.0.1
 */
class Client
{
    const FOOD_REPORT = 'reports';
    const FOOD_LIST = 'list';
    const NUTRIENT_REPORT = 'nutrients';
    const FOOD_SEARCH = 'search';

    /**
     * USDA api base url
     *
     * @var string
     */
    protected $base_url = 'http://api.nal.usda.gov/ndb';

    /**
     * USDA api key
     *
     * @var string
     */
    protected $usda_api_key;

    /**
     * GuzzleHttp client
     *
     * @var \GuzzleHttp\Client
     */
    protected $guzzle_client;

    /**
     * Client constructor.
     *
     * @param                         $usda_api_key
     * @param \GuzzleHttp\Client|null $guzzle_client
     */
    public function __construct($usda_api_key, GuzzleClient $guzzle_client = null)
    {
        $this->usda_api_key = $usda_api_key;

        // Create a new guzzle-client object when there is no external is given
        $this->guzzle_client = $guzzle_client ? $guzzle_client : new \GuzzleHttp\Client();
    }

    /**
     * Prepares the request objects for the request
     *
     * @param $request_object
     *
     * @return null|\USDAClient\Entities\SearchResponseEntity
     * @throws \USDAClient\Exceptions\USDAClientException
     */
    public function call($request_object)
    {
        if ($request_object instanceof SearchRequestEntity) {

            return $this->performCall(self::FOOD_SEARCH, $request_object->getRequestParams());
        } elseif ($request_object instanceof ReportRequestEntity) {

            return $this->performCall(self::FOOD_REPORT, $request_object->getRequestParams());
        } else {
            throw new USDAClientException('Unknown request object');
        }
    }

    /**
     * Perform a request based on type and url params
     *
     * @param $type
     * @param $urlParams
     *
     * @return null|\USDAClient\Entities\SearchResponseEntity
     * @throws \USDAClient\Exceptions\USDAClientException
     */
    protected function performCall($type, $urlParams)
    {
        $url = sprintf('%s/%s/?%s&api_key=%s',
            $this->base_url,
            $type,
            $urlParams,
            $this->usda_api_key
        );

        try {
            // perform the request
            $response = $this->guzzle_client->request('GET', $url);
        } catch (RequestException $ex) {
            $response = $ex->getResponse();
        }

        // JSON decode the body
        $response_body = json_decode($response->getBody(), true);

        if (isset($response_body['error'])) {
            throw new USDAClientException($response_body['message']);
        } elseif ($response->getStatusCode() !== 200) {
            throw new USDAClientException('USDA api response statuscode: ' . $response->getStatusCode());
        }

        return $this->createResponseObject($type, $response_body);
    }

    /**
     * Creates all the possible response objects
     *
     * @param       $type
     * @param array $response_body
     *
     * @return null|\USDAClient\Entities\SearchResponseEntity|\USDAClient\Entities\FoodReportEntity
     */
    protected function createResponseObject($type, array $response_body)
    {
        if ($type === self::FOOD_SEARCH) {
            return isset($response_body['list'])
                ? new SearchResponseEntity($response_body['list'])
                : null;
        }

        if ($type === self::FOOD_REPORT) {
            return isset($response_body['report'])
                ? new FoodReportEntity($response_body['report'])
                : null;
        }
    }

    /**
     * A little quick search function
     *
     * @param     $query
     * @param int $max
     *
     * @return null|\USDAClient\Entities\SearchResponseEntity
     */
    public function search($query, $max = 50)
    {
        $sr = new SearchRequestEntity();
        $sr->setQuery($query);
        $sr->setMax($max);
        return $this->call($sr);
    }

    /**
     * A little quick report function
     *
     * @param        $ndbno
     * @param string $type
     *
     * @return null|\USDAClient\Entities\FoodReportEntity
     */
    public function report($ndbno, $type = ReportRequestEntity::REPORT_TYPE_BASIC)
    {
        $fr = new ReportRequestEntity();
        $fr->setNdbno($ndbno);
        $fr->setType($type);
        return $this->call($fr);
    }
}